/*
 * @filename 	mfclassic.h
 * @version		0.1b, November 2015
 * @author		R.J. Rodríguez (rjrodriguez@unizar.es)
 * @description Header file describing what is needed to work properly with MIFARE Classic EEPROM format 
 *              and available functions to be invoked
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Constants of MIFARE Classic EEPROM format
#define BLOCK_SIZE 16
#define KEYA_SIZE 6
#define KEYB_SIZE 6
#define ACCCOND_SIZE 4

#define UID_SIZE 4
#define BCC_SIZE 1
#define MANUFACTURERDATA_SIZE 11

#define BLOCKSPERSHORTSECTOR 4
#define BLOCKSPERLONGSECTOR 16

#define _1KSECTORS 16
#define _4KSECTORS 40 
#define _4KSHORTSECTORS 32

#define TRUE 1
#define FALSE 0

// General types to make coding easier
typedef enum{
    MIFARE_CLASSIC_S50,
    MIFARE_CLASSIC_S70
} mfctype_t;

typedef enum{
    KEYAB,
    KEYA,
    KEYB,
    NEVER
} accesstype_t;

typedef struct _MIFAREClassic
{
    mfctype_t cardType;
    unsigned char *content;
} mfclassic_t;

typedef struct _RAWBLOCK
{
    unsigned char data[BLOCK_SIZE];
} rawblock_t;

typedef struct _TRAILERBLOCK
{
    unsigned char keyA[KEYA_SIZE];
    unsigned char accessConditions[ACCCOND_SIZE];
    unsigned char keyB[KEYB_SIZE];
} trailerblock_t;

typedef struct _MANUFACTURERBLOCK
{
    unsigned char UID[UID_SIZE];
    unsigned char BCC[BCC_SIZE];
    unsigned char manufacturerData[MANUFACTURERDATA_SIZE];
} manufacturerblock_t;

typedef struct _ACCESSCONDITIONS
{
    unsigned char block[BLOCKSPERSHORTSECTOR];
} accessconds_t;

typedef struct _WDBOPTIONS
{
    unsigned char isValueBlock;
    accesstype_t read;
    accesstype_t write;
} wdboptions_t;

typedef struct _WSTOPTIONS
{
    accesstype_t writeKeyA;
    accesstype_t readAccessBits;
    accesstype_t writeAccessBits;
    accesstype_t readKeyB;
    accesstype_t writeKeyB;
} wstoptions_t;

/**
    Reads a MIFARE Classic dump filename 
    
    @param block Raw block to be freed
    @param mfcType MIFARE Classic card type
    @return A MIFARE Classic that contains the card content and the card type
*/
mfclassic_t *readDumpFile(char *filename, mfctype_t mfcType);

/**
    Prints allowed key configuration map for a sector trailer block

Access conditions for the sector trailer
Access bits -- 	Key A         	Access bits    	Key B
C1 C2 C3 	read write 	read write 	read write
0 0 0		 never key A 	key A never 	key A key A
0 0 1		 never key A 	key A key A 	key A key A
0 1 0		 never never 	key A never 	key A never
0 1 1		 never key B 	key A|B key B 	never key B
1 0 0		 never key B 	key A|B never 	never key B
1 0 1		 never never 	key A|B key B 	never never
1 1 0		 never never 	key A|B never 	never never
1 1 1		 never never 	key A|B never 	never never
*/
void printAllowedCFGTrailerBlock();

/**
    Prints allowed key configuration map for a data block

Access bits Access condition for 						Application
C1 C2 C3 	read 	write 	increment 	decrement, 
						transfer, restore
0 0 0 		key A|B key A|B key A|B 	key A|B 			transport configuration
0 1 0 		key A|B never 	never 		never 				read/write block
1 0 0	 	key A|B key B 	never 		never 				read/write block
1 1 0 		key A|B key B 	key B 		key A|B 			value block
0 0 1 		key A|B never 	never 		key A|B 			value block
0 1 1 		key B 	key B 	never 		never 				read/write block
1 0 1 		key B 	never 	never 		never 				read/write block
1 1 1		never 	never 	never 		never				read/write block
*/
void printAllowedCFGDataBlock();

/**
    Prints a MIFARE card in binary mode
    
    @param card MIFARE card to be printed
*/
void dumpBinaryCard(mfclassic_t *card);

/**
    Reads and prints the access conditions of *all* blocks of a given MIFARE card
    
    @param card MIFARE card to be read
*/
void readAllConditions(mfclassic_t *card);

/**
    Reads and prints the access conditions of the (sector, block)
    
    @param card MIFARE card to be read
    @param sector Sector of interest
    @param block No. of block whose access conditions will be printed
*/
void readBlockConditions(mfclassic_t *card, int sector, int block);

/**
    Reads and prints the access conditions of the sector trailer of a given sector
    
    @param card MIFARE card to be read
    @param sector No. of sector whose sector trailer's access conditions will be printed
*/
void readSectorTrailerConditions(mfclassic_t *card, int sector);

/**
    Modifies the options of a given (sector, block) in a given card, and returns the modified card
    
    @param card MIFARE card to modify
    @param sector No. of sector whose data block options will be modified
    @param block No. of block inside the sector to be modified
    @param options Data block options to set 
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyDataBlockOptions(mfclassic_t *card, int sector, int block, wdboptions_t *options);

/**
    Modifies the options of a given sector trailer in a given card, and returns the modified card
    
    @param card MIFARE card to modify
    @param sector No. of sector whose sector trailer options will be modified
    @param options Sector trailer options to set 
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyTrailerBlockOptions(mfclassic_t *card, int sector, wstoptions_t *options);

/**
    Modifies the options of all data blocks in a given card, and returns the modified card
    
    @param card MIFARE card to modify
    @param options Data block options to set 
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyAllDataBlocksOptions(mfclassic_t *card, wdboptions_t *options);

/**
    Modifies the options of all sector trailers in a given card, and returns the modified card
    
    @param card MIFARE card to modify
    @param options Sector trailer options to set 
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyAllTrailerBlocksOptions(mfclassic_t *card, wstoptions_t *options);

/**
    Modifies the access key of a given sector, and returns the modified card
    
    @param card MIFARE card to modify
    @param sector No. of sector whose sector trailer options will be modified
    @param key Key to modify, either KEYA or KEYB
    @param hexkey 6-byte hex string -- assumed to be valid!
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifySectorKey(mfclassic_t *card, int sector, accesstype_t key, unsigned char *hexkey);

/**
    Modifies the access key of all sectors, and returns the modified card
    
    @param card MIFARE card to modify
    @param key Key to modify, either KEYA or KEYB
    @param hexkey 6-byte hex string -- assumed to be valid!
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyAllSectorsKey(mfclassic_t *card, accesstype_t key, unsigned char *hexkey);

/**
    Writes a MIFARE card to a file
    
    @param outfilename Filename of output file
    @param card MIFARE card to dump
*/
void writeCardToFile(char *outfilename, mfclassic_t *card);


