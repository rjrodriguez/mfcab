/*
 * @filename 		mfcab.c	
 * @version			0.1b, November 2015
 * @author			R.J. Rodríguez (rjrodriguez@unizar.es)
 * @description	
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//TODO Añadir DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mfclassic.h"


void dumpCard(char *filename, mfctype_t cardType)
{
    mfclassic_t *newCard = readDumpFile(filename, cardType);
    dumpBinaryCard(newCard);
}

void writeOutputAllST(char *dumpfilename, mfctype_t cardType, 
                        char *outfilename, wstoptions_t *options)
{
    mfclassic_t *oldCard = readDumpFile(dumpfilename, cardType);
    if(oldCard != NULL)
    {
        mfclassic_t *newCard = modifyAllTrailerBlocksOptions(oldCard, options);
        writeCardToFile(outfilename, newCard);
    }
}

void writeOutputAllDB(char *dumpfilename, mfctype_t cardType, 
                        char *outfilename, wdboptions_t *options)
{
    mfclassic_t *oldCard = readDumpFile(dumpfilename, cardType);
    if(oldCard != NULL)
    {
        mfclassic_t *newCard = modifyAllDataBlocksOptions(oldCard, options);
        writeCardToFile(outfilename, newCard);
    }
}

void writeOutputTrailerBlock(char *dumpfilename, mfctype_t cardType, 
                        char *outfilename, int sector, wstoptions_t *options)
{
    mfclassic_t *oldCard = readDumpFile(dumpfilename, cardType);
    if(oldCard != NULL)
    {
        mfclassic_t *newCard = modifyTrailerBlockOptions(oldCard, sector, options);
        writeCardToFile(outfilename, newCard);
    }
}


void writeOutputDataBlock(char *dumpfilename, mfctype_t cardType, 
                        char *outfilename, int sector, int block, wdboptions_t *options)
{
    mfclassic_t *oldCard = readDumpFile(dumpfilename, cardType);
    if(oldCard != NULL)
    {
        mfclassic_t *newCard = modifyDataBlockOptions(oldCard, sector, block, options);
        writeCardToFile(outfilename, newCard);
    }
}

void readAllBlocks(char *filename, mfctype_t cardType)
{
    mfclassic_t *newCard = readDumpFile(filename, cardType);
    readAllConditions(newCard);
}

void readBlock(char *filename, mfctype_t cardType, int sector, int block)
{
    mfclassic_t *newCard = readDumpFile(filename, cardType);
    readBlockConditions(newCard, sector, block);
}

void readSectorTrailer(char *filename, mfctype_t cardType, int sector)
{
    mfclassic_t *newCard = readDumpFile(filename, cardType);
    readSectorTrailerConditions(newCard, sector);
}

int isValidHexString(char *hexString, short len)
{ 
    int isValid = TRUE;

    for(int i = 0; i < len && isValid; i++)
        isValid &= (hexString[i] >= '0' && hexString[i] <= '9') ||
                    (hexString[i] >= 'a' && hexString[i] <= 'f') ||
                    (hexString[i] >= 'A' && hexString[i] <= 'F');

    return isValid;
}

int hexValue(char value)
{
    if(value >= '0' && value <= '9')
        return value - '0';
    else
        return tolower(value) - 'a' + 10;
}

void writeSectorKey(char *dumpfilename, mfctype_t cardType, 
                        char *outfilename, int sector, 
                        accesstype_t key, char *auxKey)
{
    unsigned char newKey[KEYA_SIZE];
    // Build the key
    for(int i = 0; i < KEYA_SIZE; i++)
        newKey[i] = ((hexValue(auxKey[2*i]) << 4 )+ 
                        hexValue(auxKey[2*i + 1]));
                        
    mfclassic_t *oldCard = readDumpFile(dumpfilename, cardType);
    if(oldCard != NULL)
    {
        mfclassic_t *newCard = modifySectorKey(oldCard, sector, key, newKey);
        writeCardToFile(outfilename, newCard);
    }
}

void writeAllSectorsKey(char *dumpfilename, mfctype_t cardType, char *outfilename,
                        accesstype_t key, char *auxKey)
{
    unsigned char newKey[KEYA_SIZE];
    // Build the key
    for(int i = 0; i < KEYA_SIZE; i++)
        newKey[i] = ((hexValue(auxKey[2*i]) << 4 )+ 
                        hexValue(auxKey[2*i + 1]));
                        
    mfclassic_t *oldCard = readDumpFile(dumpfilename, cardType);
    if(oldCard != NULL)
    {
        mfclassic_t *newCard = modifyAllSectorsKey(oldCard, key, newKey);
        writeCardToFile(outfilename, newCard);
    }
}

/**
    Prints the program usage, showing all parameters
    
    @param programName Program name
*/
void printUsage(char* programName)
{
    printf("usage:\t%s [<GENERAL OPTIONS> | [COMMON OPTIONS, <READ OPTIONS | WRITE OPTIONS>]]\n\n", programName);
    printf("General options:\n");
    printf("\t-h\t| --help\t\t\tprints this help\n");
    printf("\t-vdb\t| --valid-data-block\t\tshows valid access conditions for data blocks\n");
    printf("\t-vst\t| --valid-sector-trailer\tshows valid access conditions for sector trailer blocks\n");
    printf("\nCommon options:\n");
    printf("\t-f\t| --file DUMPFILENAME\t\tspecifies the filename that contains the card dump\n");
    printf("\t-t\t| --type <S50,S70>\t\tspecifies the type of the card. S50 means MFC1K, S70 means MFC4K\n");
    printf("\nRead options:\n");
    printf("\t-d\t| --dump\t\t\tdump the card, in binary form\n");
    printf("\t-rall\t| --read-all\t\t\tread access conditions of all blocks of the card dump\n");
    printf("\t-r\t| --read SEC,BLK\t\tread access conditions of block BLK of sector SEC\n");
    printf("\t-rst\t| --read-trailer SEC\t\tread access conditions and keys of sector trailer SEC\n");
    printf("\nWrite options:\n");
    printf("\t-o\t| --out OUTFILENAME\t\twrite card dump with acess bits modified to OUTFILENAME\n");
    printf("\t-walldb\t| --write-all-db WDBOPTIONS\twrite all data blocks with access conditions as specified by WDBOPTIONS (see below)\n");
    printf("\t-wdb\t| --write-db SEC,BLK WDBOPTIONS\twrite data block BLK of sector SEC with access conditions as specified by WDBOPTIONS (see below)\n");
    printf("\t-wallst\t| --write-all-st WSTOPTIONS\twrite all sector trailers with access conditions as specified by WSTOPTIONS (see below)\n");
    printf("\t-wst\t| --write-st SEC WSTOPTIONS\twrite sector trailer SEC with access conditions as specified by WSTOPTIONS (see below)\n");
    printf("\t-wka\t| --write-key-a SEC KEYA\twrite key A for sector SEC. KEYA must be a 6-byte hex string\n");
    printf("\t-wkb\t| --write-key-b SEC KEYB\twrite key B for sector SEC. KEYB must be a 6-byte hex string\n");
    printf("\t-waka\t| --write-all-key-a KEYA\twrite key A for all card sectors. KEYA must be a 6-byte hex string\n");
    printf("\t-wakb\t| --write-all-key-b KEYB\twrite key B for all card sectors. KEYB must be a 6-byte hex string\n");

    printf("\nWBDOPTIONS: ");
    printf("[rw|v,rdb*,wdb*]\n");
    printf("\trw\t\tdata block is a read/write block\n");
    printf("\tv\t\tdata block is a value block\n");
    printf("\trdb\t\tread data block\n");
    printf("\twdb\t\twrite data block\n");
    
    printf("\nWSTOPTIONS: ");
    printf("[wka,rab,wab,rkb,wkb]\n");
    printf("\twka\t\twrite key A\n");
    printf("\trab\t\tread access bits (only valid A: key A; AB: key A or key B)\n");
    printf("\twab\t\twrite access bits\n");
    printf("\trkb\t\tread key B\n");
    printf("\twkb\t\twrite key B\n");
    printf("Possible values for WBOPTIONS* and WSTOPTIONS -- N: never, A: key A, B: key B, AB: key A or key B\n");
}

/**
    Checks if the input parameters are correct
    
    @param filename String of filename
    @param type Card type correctly read 
    @return TRUE when parameters are correct, FALSE otherwise.
*/
int checkDumpTypeParameters(char *filename, char type)
{
    return (filename != NULL && type);
}

/**
    Checks if the output parameters are correct
    
    @param filename String of filename
    @param options Options 
    @return TRUE when parameters are correct, FALSE otherwise.
*/
int checkOutputParameters(char *filename, void *options)
{
        return (filename!= NULL && options != NULL);
}

/**
    Parses the options for data block from a given string.
    Option values can be "N", "A", "B", or "AB"
    
    @param options String to parse
    @return A wdboptions_t containing the options after parsing
*/
wdboptions_t *readWriteDataBlockOptions(char *options)
{
    wdboptions_t *wdboptions = calloc(1, sizeof(wdboptions_t));

    // Get type of block
    char *auxStr = strtok(options, ",");
    if(!strcmp(auxStr, "rw"))
        wdboptions -> isValueBlock = FALSE;
    else if(!strcmp(auxStr, "v"))    
        wdboptions -> isValueBlock = TRUE;
    else
    {
        free(wdboptions);
        return NULL;
    }

    // Get read key
    auxStr = strtok(NULL, ",");    
    if(!strcmp(auxStr, "N"))
        wdboptions -> read = NEVER;
    else if(!strcmp(auxStr, "A"))
        wdboptions -> read = KEYA;
    else if(!strcmp(auxStr, "B"))
        wdboptions -> read = KEYB;
    else if(!strcmp(auxStr, "AB"))
        wdboptions -> read = KEYAB;
    else
    {
        free(wdboptions);
        return NULL;
    }
    
    // Get write key
    auxStr = strtok(NULL, ",");    
    if(!strcmp(auxStr, "N"))
        wdboptions -> write = NEVER;
    else if(!strcmp(auxStr, "A"))
        wdboptions -> write = KEYA;
    else if(!strcmp(auxStr, "B"))
        wdboptions -> write = KEYB;
    else if(!strcmp(auxStr, "AB"))
        wdboptions -> write = KEYAB;
    else
    {
        free(wdboptions);
        return NULL;
    }

    return wdboptions;
}

/**
    Parses the options for sector trailer block from a given string.
    Option values can be "N", "A", "B", or "AB"
    
    @param options String to parse
    @return A wstoptions_t containing the options after parsing
*/
wstoptions_t *readWriteTrailerBlockOptions(char *options)
{
    wstoptions_t *wstoptions = calloc(1, sizeof(wstoptions_t));

    char *auxStr = strtok(options, ",");
    // Generic pointer
    accesstype_t *auxPtr = &(wstoptions -> writeKeyA);
    int i;
    for(i = 0; i < sizeof(wstoptions_t)/sizeof(accesstype_t) && auxStr != NULL; i++)
    {
        //fprintf(stdout, "%x, %s\n", auxPtr, auxStr);
        if(!strcmp(auxStr, "N"))
            *auxPtr = NEVER;
        else if(!strcmp(auxStr, "A"))
            *auxPtr = KEYA;
        else if(!strcmp(auxStr, "B"))
            *auxPtr = KEYB;
        else if(!strcmp(auxStr, "AB"))
            *auxPtr = KEYAB;
        else
        {
            free(wstoptions);
            return NULL;
        }

        auxPtr++;// sizeof(accesstype_t); 
        // Get next value
        auxStr = strtok(NULL, ",");
    }

    return wstoptions;
}

/**
    Parses the sector, block information from a given string. Sector, block must be integer values, no checking is done here guys
    
    @param string String to parse
    @param sector Pointer to sector integer variable 
    @param block Pointer to block integer variable
    @return TRUE when the parsing has been correct, FALSE otherwise.
*/
int readSectorBlockOptions(char *string, int *sector, int *block)
{
    char *auxStr = strtok(string, ",");
    *sector = atoi(auxStr);
    auxStr = strtok(NULL, ",");
    if(auxStr == NULL)
        return FALSE;
    *block = atoi(auxStr);

    return TRUE;
}

/**
    Parses the command-line input parameters
    
    @param nArgs Number of input parameters
    @param args Array of input parameters 
    @return TRUE when the parsing has been correct, FALSE otherwise.
*/
int readParameters(int nArgs, char *args[])
{
    int i;
    char *dumpfilename = NULL,
         *outfilename = NULL;
    mfctype_t cardType;
    int auxSector, auxBlock;
    char cardTypeOK = FALSE;

    // Process all parameters
    for(i = 1; i < nArgs; i++)
    {
        if(!strcmp(args[i], "-h") || !strcmp(args[i], "--help"))
        {
            printUsage(args[0]);
            return TRUE;
        }else if(!strcmp(args[i], "-vdb") || !strcmp(args[i], "--valid-data-block"))
        {
            printAllowedCFGDataBlock();
            return TRUE;
        }else if(!strcmp(args[i], "-vst") || !strcmp(args[i], "--valid-sector-trailer"))
        {
            printAllowedCFGTrailerBlock();
            return TRUE;
        }else if(!strcmp(args[i], "-f") || !strcmp(args[i], "--file"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL)
                return FALSE;
            else{
                int len = strlen(args[i]);
                dumpfilename = calloc(len, sizeof(char));
                strncpy(dumpfilename, args[i], len);
            }
        }else if(!strcmp(args[i], "-t") || !strcmp(args[i], "--type"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL)
                return FALSE;
            else if(!strcmp(args[i], "S50"))
            {
                cardType = MIFARE_CLASSIC_S50;
                cardTypeOK = TRUE;
            }
            else if(!strcmp(args[i], "S70"))
            {
                cardType = MIFARE_CLASSIC_S70;
                cardTypeOK = TRUE;
            }
            else
                return FALSE;
        }else if(!strcmp(args[i], "-d") || !strcmp(args[i], "--dump"))
        {
            if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                return FALSE;
            else
            {
                dumpCard(dumpfilename, cardType);
                return TRUE;
            } 
        }else if(!strcmp(args[i], "-rall") || !strcmp(args[i], "--read-all"))
        {
            if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                return FALSE;
            else
            {
                readAllBlocks(dumpfilename, cardType);
                return TRUE;
            } 
        }else if(!strcmp(args[i], "-r") || !strcmp(args[i], "--read"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL)
                return FALSE;

            if(!readSectorBlockOptions(args[i], &auxSector, &auxBlock))
                return FALSE;

            if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                return FALSE;
            else
            {
                readBlock(dumpfilename, cardType, auxSector, auxBlock);
                return TRUE;
            } 

        }else if(!strcmp(args[i], "-rst") || !strcmp(args[i], "--read-trailer"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL)
                return FALSE;

            auxSector = atoi(args[i]);
            
            if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                return FALSE;
            else
            {
                readSectorTrailer(dumpfilename, cardType, auxSector);
                return TRUE;
            } 
        }else if(!strcmp(args[i], "-o") || !strcmp(args[i], "--out"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL)
                return FALSE;
            else{
                int len = strlen(args[i]);
                outfilename = calloc(len, sizeof(char));
                strncpy(outfilename, args[i], len);
            }
        }else if(!strcmp(args[i], "-walldb") || !strcmp(args[i], "--write-all-db"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL)
                return FALSE;

            // Read data block options to be written
            wdboptions_t *wdboptions = readWriteDataBlockOptions(args[i]);

            if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                return FALSE;
            else if(checkOutputParameters(outfilename, wdboptions))
            {
                writeOutputAllDB(dumpfilename, cardType, outfilename, wdboptions);
                return TRUE;
            }
        }else if(!strcmp(args[i], "-wdb") || !strcmp(args[i], "--write-db"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL || args[i + 1] == NULL)
                return FALSE;
                
            if(!readSectorBlockOptions(args[i++], &auxSector, &auxBlock))
                return FALSE;

            // Read data block options to be written
            wdboptions_t *wdboptions = readWriteDataBlockOptions(args[i]);
 
            if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                return FALSE;
            else if(checkOutputParameters(outfilename, wdboptions))
            {
                writeOutputDataBlock(dumpfilename, cardType, outfilename, auxSector, auxBlock, wdboptions);
                return TRUE;
            }
        }else if(!strcmp(args[i], "-wallst") || !strcmp(args[i], "--write-all-st"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL)
                return FALSE;

            // Read sector trailer block options to be written
            wstoptions_t *wstoptions = readWriteTrailerBlockOptions(args[i]);

            if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                return FALSE;
            else if(checkOutputParameters(outfilename, wstoptions))
            {
                writeOutputAllST(dumpfilename, cardType, outfilename, wstoptions);
                return TRUE;
            }
        }else if(!strcmp(args[i], "-wst") || !strcmp(args[i], "--write-st"))
        {
            // Check whether parameter is valid
            if(args[++i] == NULL || args[i + 1] == NULL)
                return FALSE;

            auxSector = atoi(args[i++]);
                
            // Read sector trailer block options to be written
            wstoptions_t *wstoptions = readWriteTrailerBlockOptions(args[i]);
            
            if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                return FALSE;
            else if(checkOutputParameters(outfilename, wstoptions))
            {
                writeOutputTrailerBlock(dumpfilename, cardType, outfilename, auxSector, wstoptions);
                return TRUE;
            }
        }else if(!strcmp(args[i], "-wka") || !strcmp(args[i], "--write-key-a") ||
                    (!strcmp(args[i], "-wkb") || !strcmp(args[i], "--write-key-b")))
        {
            accesstype_t key2modify;
            if(!strcmp(args[i], "-wka") || !strcmp(args[i], "--write-key-a"))
                key2modify = KEYA;
            else
                key2modify = KEYB;
        
            // Check whether parameter is valid
            if(args[++i] == NULL || args[i + 1] == NULL)
                return FALSE;

            auxSector = atoi(args[i++]);
            
            // Check if the hex string is valid
            if(!isValidHexString(args[i], strlen(args[i])) || strlen(args[i]) != KEYA_SIZE*2){ // XXX Assumed KEYA_SIZE will be always equal to KEYB_SIZE
                return FALSE;
            }else
            {
                if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                    return FALSE;
                else if(outfilename != NULL)
                {
                    writeSectorKey(dumpfilename, cardType, outfilename,
                                     auxSector, key2modify, args[i]);    
                    return TRUE;
                }
            }
        }else if(!strcmp(args[i], "-waka") || !strcmp(args[i], "--write-all-key-a") ||
                    (!strcmp(args[i], "-wakb") || !strcmp(args[i], "--write-all-key-b")))
        {
            accesstype_t key2modify;
            if(!strcmp(args[i], "-waka") || !strcmp(args[i], "--write-all-key-a"))
                key2modify = KEYA;
            else
                key2modify = KEYB;
        
            // Check whether parameter is valid
            if(args[++i] == NULL)
                return FALSE;

            // Check if the hex string is valid
            if(!isValidHexString(args[i], strlen(args[i])) || strlen(args[i]) != KEYA_SIZE*2){ // XXX Assumed KEYA_SIZE will be always equal to KEYB_SIZE
                return FALSE;
            }else
            {
                if(!checkDumpTypeParameters(dumpfilename, cardTypeOK))
                    return FALSE;
                else if(outfilename != NULL)
                {
                    writeAllSectorsKey(dumpfilename, cardType, outfilename, key2modify, args[i]);    
                    return TRUE;
                }
            }
        } // end processings of parameters
    } // end for

    return FALSE;
}


int main(int argc, char *argv[])
{
    if(!readParameters(argc, argv))
    {
        fprintf(stderr, "ERROR: invalid parameters\n");
        printUsage(argv[0]);
        exit(-1);
    }
	
    return 0;
}
