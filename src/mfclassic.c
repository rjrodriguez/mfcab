/*
 * @filename 	mfclassic.c
 * @version		0.1b, November 2015
 * @author		R.J. Rodríguez (rjrodriguez@unizar.es)
 * @description		
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//TODO Add DEBUG

#include "mfclassic.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define KEYA_COLOR      ANSI_COLOR_RED
#define ACCCOND_COLOR   ANSI_COLOR_YELLOW
#define KEYB_COLOR      ANSI_COLOR_MAGENTA
#define WARNING_COLOR   ANSI_COLOR_GREEN


/**
    Returns the maximum number of bytes specified by the card type
    
    @param mfcType MIFARE Classic card type
    @return The length in bytes of the EEPROM specified by the card type
*/
int getCardTypeMaxBytes(mfctype_t mfcType)
{
    return (mfcType == MIFARE_CLASSIC_S50 ? 
                                (BLOCK_SIZE*BLOCKSPERSHORTSECTOR*_1KSECTORS):
                                (BLOCK_SIZE*(BLOCKSPERSHORTSECTOR*_4KSHORTSECTORS + 
                                        (_4KSECTORS - _4KSHORTSECTORS)*BLOCKSPERLONGSECTOR)));
}

/**
    Returns the number of sector trailer block, depending on the card type and the sector
    
    @param mfcType MIFARE Classic card type
    @param sector Sector of interest
    @return The number of sector trailer block, depending on the card type
*/
int getSectorTrailerBlockNum(mfctype_t mfcType, int sector)
{
    int aux =  (BLOCKSPERSHORTSECTOR - 1);
    if(mfcType == MIFARE_CLASSIC_S50)
        return aux;
    else
        return (sector < _4KSHORTSECTORS ? aux : BLOCKSPERLONGSECTOR);
}

/**
    Returns a string indicating the card type textually
    
    @param mfcType MIFARE Classic card type
    @return The string
*/
char *getCardTypeString(mfctype_t cardType)
{
    switch(cardType)
    {
        case MIFARE_CLASSIC_S50:
            return "MIFARE Classic S50 1K";
        case MIFARE_CLASSIC_S70:
            return "MIFARE Classic S70 4K";
        default:
            return "I don't know what the f**k type card you provide ¿?";
    }
}

/**
    Releases the memory acquired by the card
    
    @param card MIFARE Classic card type to be freed
*/
void releaseCard(mfclassic_t *card)
{
    if(card == NULL)
    {
        fprintf(stderr, "ERROR card already released?\n");
        return;
    }else
    {
        // Free memory
        free(card -> content);
        free(card);
    }
}

/**
    Releases the memory allocated by a raw block
    
    @param block Raw block to be freed
*/
void releaseRawBlock(rawblock_t *block)
{
    if(block == NULL)
    {
        fprintf(stderr, "ERROR raw block already released?\n");
        return;
    }else
        // Free memory
        free(block);
}

/**
    Reads a MIFARE Classic dump filename 
    
    @param block Raw block to be freed
    @param mfcType MIFARE Classic card type
    @return A MIFARE Classic that contains the card content and the card type
*/
mfclassic_t *readDumpFile(char *filename, mfctype_t mfcType)
{
    FILE *f = fopen(filename, "r");
    int bytesToRead;
    unsigned char data;

    if(f == NULL)
    {
        fprintf(stderr, "ERROR when opening [%s] file. Does it exist?\n", filename);
        return NULL;
    }
    mfclassic_t *newMFCcard = calloc(1, sizeof(mfclassic_t));
    
    newMFCcard -> cardType = mfcType;
    
    bytesToRead = getCardTypeMaxBytes(mfcType);
    // Allocate memory
    newMFCcard -> content = calloc(1, sizeof(unsigned char)*bytesToRead);
   
    fprintf(stdout, "Reading filename [%s] (type: %s)\n", filename, getCardTypeString(mfcType));

    int i = 0;
    while(!feof(f) && i <= bytesToRead)
    {
        fread(&data, sizeof(unsigned char), 1, f);
        newMFCcard -> content[i++] = data;
        if(ferror(f))
        {
            fprintf(stderr, "Read error\n");
            break;
        }
    }

    if(!feof(f))
        fprintf(stderr, WARNING_COLOR "(!!) WARNING: It seems file length is different than card type specified\n" ANSI_COLOR_RESET);
    if(feof(f) && i <= bytesToRead)
        fprintf(stderr, WARNING_COLOR "(!!) WARNING: Card type specified as parameter oversizes file length\n" ANSI_COLOR_RESET);

    fclose(f);

    return newMFCcard;
}

/**
    Prints data block legend
*/
void printDatablockLegend()
{
    printf("r: read; w: write; i: increment; d: decrement; t: transfer; rs: restore\n");
    printf("*: value block(s) (otherwise, r/w block(s))\n");
}

/**
    Prints sector trailer block legend
*/
void printSectorTrailerLegend()
{
    printf("kaw: Key A write; abr: Access bits read; abw: Access bits write; kbr: Key B read; kbw: Key B write\n");
}

/**
    Prints key legend (allowed chars when setting also)
*/
void printKeyLegend()
{
    printf("LEGEND: A (key A); B (key B); AB (key A or key B); N (never)\n");
}

/**
    Prints legend, used when showing the valid configurations for data and sector trailer blocks
    
    @param datablockLegend Boolean value to indicate whether the data block legend must be shown
    @param sectorTrailerLegend Boolean value to indicate whether the sector trailer block legend must be shown
*/
void printLegend(int datablockLegend, int sectorTrailerLegend)
{
    printf("-------------------------------------\n");
    printKeyLegend();
    if(datablockLegend)
        printDatablockLegend();
    if(sectorTrailerLegend)
        printSectorTrailerLegend();
    printf("-------------------------------------\n");
}


/**
    Prints allowed key configuration map for a sector trailer block

Access conditions for the sector trailer
Access bits -- 	Key A         	Access bits    	Key B
C1 C2 C3 	read write 	read write 	read write
0 0 0		 never key A 	key A never 	key A key A
0 0 1		 never key A 	key A key A 	key A key A
0 1 0		 never never 	key A never 	key A never
0 1 1		 never key B 	key A|B key B 	never key B
1 0 0		 never key B 	key A|B never 	never key B
1 0 1		 never never 	key A|B key B 	never never
1 1 0		 never never 	key A|B never 	never never
1 1 1		 never never 	key A|B never 	never never
*/
void printAllowedCFGTrailerBlock()
{
    printf("Valid map of access conditions for sector trailer\n");
    printf("-------------------------------------\n");
    printf("\tKEY A\t\tAccess bits\t\tKEY B\n");
    printf("\twrite\t\tread\twrite\t\tread\twrite\n");
    printf("-------------------------------------\n");
    printf("\tA\t\tA\tN\t\tA\tA\n");
    printf("\tA\t\tA\tA\t\tA\tA\n");
    printf("\tN\t\tA\tN\t\tA\tN\t\t**not advised\n");
    printf("\tB\t\tAB\tB\t\tN\tB\n");
    printf("\tB\t\tAB\tN\t\tN\tB\t\t**not advised\n");
    printf("\tN\t\tAB\tB\t\tN\tN\n");
    printf("\tN\t\tAB\tN\t\tN\tN\t\t**not advised\n");
    printLegend(FALSE, FALSE);
}

/**
    Prints allowed key configuration map for a data block

Access bits Access condition for 						Application
C1 C2 C3 	read 	write 	increment 	decrement, 
						transfer, restore
0 0 0 		key A|B key A|B key A|B 	key A|B 			transport configuration
0 1 0 		key A|B never 	never 		never 				read/write block
1 0 0	 	key A|B key B 	never 		never 				read/write block
1 1 0 		key A|B key B 	key B 		key A|B 			value block
0 0 1 		key A|B never 	never 		key A|B 			value block
0 1 1 		key B 	key B 	never 		never 				read/write block
1 0 1 		key B 	never 	never 		never 				read/write block
1 1 1		never 	never 	never 		never				read/write block
*/
void printAllowedCFGDataBlock()
{
    printf("Valid map of access conditions for data blocks\n");
    printf("-------------------------------------\n");
    printf("\tread\twrite\t\tBlock type\n");
    printf("-------------------------------------\n");
    printf("\tAB\tAB\t\tr/w block\n");
    printf("\tAB\tN\t\tr/w block\n");
    printf("\tAB\tB\t\tr/w block\n");
    printf("\tAB\tB\t\tvalue block -- allows increment, decrement, etc. with AB\n");
    printf("\tAB\tN\t\tvalue block -- allows decrement, etc. with AB\n");
    printf("\tB\tB\t\tr/w block\n");
    printf("\tB\tN\t\tr/w block\t\t\t\t**not advised\n");
    printf("\tN\tN\t\tr/w block\t\t\t\t**not advised\n");
    printLegend(FALSE, FALSE);
}

/**
    Returns the init address of a sector in a given type of MIFARE card
    
    @param cardType MIFARE Classic card type
    @param sector No. of sector of interest
    @return An integer representing the init address of the sector for a given MIFARE card type
*/
int computeSectorInitAddress(mfctype_t cardType, int sector)
{
    unsigned int MAX_SMALLBLOCKS = (cardType == MIFARE_CLASSIC_S50 ? _1KSECTORS : _4KSHORTSECTORS);
    unsigned int initAddress = 0;
  
    if(sector < MAX_SMALLBLOCKS)
    {
        initAddress = BLOCK_SIZE*sector*BLOCKSPERSHORTSECTOR; // 4 blocks per sector
    }else
    {
        initAddress = BLOCK_SIZE*MAX_SMALLBLOCKS*BLOCKSPERSHORTSECTOR; // 4 blocks per sector
        // Compute now the offset to the new sector
        initAddress += BLOCK_SIZE*(sector - MAX_SMALLBLOCKS)*BLOCKSPERSHORTSECTOR; // Remaining sectors are 16 block-length
    }

    return initAddress;
}

/**
    Returns the init address of a block of a sector in a given type of MIFARE card
    
    @param cardType MIFARE Classic card type
    @param sector No. of sector of interest
    @param block No. of block of interest
    @return An integer representing the init address of the block, sector for a given MIFARE card type
*/
int computeBlockInitAddress(mfctype_t cardType, int sector, int block)
{
    return computeSectorInitAddress(cardType, sector) + block*BLOCK_SIZE;
}

/**
    Returns the init address of a sector trailer of the given sector in a given type of MIFARE card
    
    @param cardType MIFARE Classic card type
    @param sector No. of sector of interest
    @return An integer representing the init address of the sector trailer of a sector for a given MIFARE card type
*/
int computeSectorTrailerInitAddress(mfctype_t cardType, int sector)
{

    return (sector < (cardType == MIFARE_CLASSIC_S50 ? _1KSECTORS : _4KSHORTSECTORS)) ? 
        computeBlockInitAddress(cardType, sector, (BLOCKSPERSHORTSECTOR - 1)):
        computeBlockInitAddress(cardType, sector, (BLOCKSPERLONGSECTOR - 1));
    
}

/**
    Returns the maximum number of sectors for a given card type
    
    @param cardType MIFARE Classic card type
    @return An integer representing the maximum number of sectors for a given card type
*/
int getMaxSectors(mfctype_t cardType)
{
    return (cardType == MIFARE_CLASSIC_S50 ? _1KSECTORS: _4KSECTORS);
}


/**
    Validates the block of a given sector for a given type of MIFARE card
    
    @param cardType MIFARE Classic card type
    @param sector No. of sector of interest
    @param block No. of block of interest
    @return TRUE when (block, sector) are valid for a given MIFARE card type; FALSE otherwise
*/
int validateSectorBlock(mfctype_t cardType, int sector, int block)
{
    // 16 sectors in S50; 40 sectors in S70 
    unsigned int MAX_SECTORS = getMaxSectors(cardType);
  
    if(!(sector >= 0 && sector < MAX_SECTORS)){
        fprintf(stderr, "validateSectorBlock: sector parameter invalid (%d)\n", sector);
        return FALSE;
    }
    // Check the block number now
    if(!((sector < _4KSHORTSECTORS && (block >= 0 && block < BLOCKSPERSHORTSECTOR)) || 
        (sector >= _4KSHORTSECTORS && (block >= 0 && block < BLOCKSPERLONGSECTOR))))
    {
        fprintf(stderr, "validateSectorBlock: block parameter invalid (%d, %d)\n", sector, block);
        return FALSE;
    }
    
    return TRUE;
}

/**
    Validates the sector for a given type of MIFARE card
    
    @param cardType MIFARE Classic card type
    @param sector No. of sector to validate
    @return TRUE when sector is valid for a given MIFARE card type; FALSE otherwise
*/
int validateSector(mfctype_t cardType, int sector)
{
    // 16 sectors in S50; 40 sectors in S70 
    unsigned int MAX_SECTORS = getMaxSectors(cardType);
  
    if(!(sector >= 0 && sector < MAX_SECTORS)){
        fprintf(stderr, "validateSectorBlock: sector parameter invalid (%d)\n", sector);
        return FALSE;
    }

    return TRUE;
}

/**
    Checks whether the (sector, block) is a data block
    
    @param cardType MIFARE Classic card type
    @param sector No. of sector of interest
    @param block No. of block of interest
    @return TRUE when block is a data block in sector for a given MIFARE card type; FALSE otherwise
*/
int isDataBlock(mfctype_t cardType, int sector, int block)
{
    if(!validateSectorBlock(cardType, sector, block))
        return FALSE;
    
    return (sector < _4KSHORTSECTORS && (block >= 0 && block < (BLOCKSPERSHORTSECTOR - 1))) || 
        (sector >= _4KSHORTSECTORS && (block >= 0 && block < (BLOCKSPERLONGSECTOR - 1)));
}

/**
    Checks whether the (sector, block) is a trailer block
    
    @param cardType MIFARE Classic card type
    @param sector No. of sector of interest
    @param block No. of block of interest
    @return TRUE when block is a trailer block in sector for a given MIFARE card type; FALSE otherwise
*/
int isTrailerBlock(mfctype_t cardType, int sector, int block)
{
    if(!validateSectorBlock(cardType, sector, block))
        return FALSE;

    return (block == getSectorTrailerBlockNum(cardType, sector));
}


/**
    Prints a trailer block (in hexadecimal)
    
    @param block Trailer block to be printed
*/
void printTrailerBlock(trailerblock_t *block)
{
    int i;
    printf("[KEY A: " KEYA_COLOR);
    for(i = 0; i < KEYA_SIZE; i++)
        printf("%02x", block -> keyA[i]);
    printf(ANSI_COLOR_RESET "; ACCESS CONDITIONS: " ACCCOND_COLOR);
    for(i = 0; i < ACCCOND_SIZE; i++)
        printf("%02x", block -> accessConditions[i]);
    printf(ANSI_COLOR_RESET "; KEY B: " KEYB_COLOR);
    for(i = 0; i < KEYB_SIZE; i++)
        printf(KEYB_COLOR "%02x", block -> keyB[i]);
    printf(ANSI_COLOR_RESET "]\n");
}

/**
    Prints a raw block (in hexadecimal)
    
    @param block Raw block to be printed
*/
void printRawBlock(rawblock_t *block)
{
    int i;
    printf("****\n");
    for(i = 0; i < BLOCK_SIZE; i++)
        printf("%x", block -> data[i]);
    printf("\n****\n");
}

/**
    Prints the manufacturer block (in hexadecimal)

    @param block Manufacturer block to be printed
*/
void printManufacturerBlock(manufacturerblock_t *block)
{
    int i;
    printf("UID: ");
    for(i = 0; i < UID_SIZE; i++)
        printf("%02x", block -> UID[i]);
    printf("; BCC: ");
    for(i = 0; i < BCC_SIZE; i++)
        printf("%02x", block -> BCC[i]);
    printf("; Manufacturer data: ");
    for(i = 0; i < MANUFACTURERDATA_SIZE; i++)
        printf("%02x", block -> manufacturerData[i]);
}

/**
    Returns a manufacturer block of a given MIFARE card

    @param card MIFARE card to parse
    @return The manufacturer block of the MIFARE card
*/
manufacturerblock_t *buildManufacturerBlock(mfclassic_t *card)
{
    manufacturerblock_t *manufacturerBlock = calloc(1, sizeof(manufacturerblock_t));
    
    int i, j = 0, aux = 0;
    for(i = 0; i < UID_SIZE; i++)
    {
        aux ^= card -> content[j];
        manufacturerBlock -> UID[i] = card -> content[j++];
    }
    for(i = 0; i < BCC_SIZE; i++)
        manufacturerBlock -> BCC[i] = card -> content[j++];
    for(i = 0; i < MANUFACTURERDATA_SIZE; i++)
        manufacturerBlock -> manufacturerData[i] = card -> content[j++];
    
    // Before exiting, check whether BCC value is correct
    if(aux != manufacturerBlock -> BCC[0]) // Only one byte in fact
        fprintf(stderr, WARNING_COLOR "(!!) WARNING: BCC value incorrect (expected %x, but found %x)!\n" ANSI_COLOR_RESET, aux, manufacturerBlock -> BCC[0]);

    return manufacturerBlock;
}

/**
    Returns a raw block of a given MIFARE card, from a given address

    @param card MIFARE card to parse
    @param fromAddress Init address to begin the reading of the block
    @return The raw block of the MIFARE card
*/
rawblock_t *buildRawBlock(mfclassic_t *card, int fromAddress)
{
    int i;
    rawblock_t *newBlock = calloc(1, sizeof(rawblock_t));
    
    for(i = 0; i < BLOCK_SIZE; i++)
        newBlock -> data[i] = card -> content[fromAddress + i];
    
    return newBlock;
}

/**
    Returns a trailer block of a given MIFARE card, from a given address

    @param card MIFARE card to parse
    @param fromAddress Init address to begin the reading of the block
    @return The trailer block of the MIFARE card
*/
trailerblock_t *buildTrailerBlock(mfclassic_t *card, int fromAddress)
{
    int i, j;
    trailerblock_t *newTrailerBlock = calloc(1, sizeof(trailerblock_t));
    
    for(i = 0; i < 6; i++)
        newTrailerBlock -> keyA[i] =  card -> content[fromAddress + i];
    for(i = j = 6; i < 10; i++)
        newTrailerBlock -> accessConditions[i - j] =  card -> content[fromAddress + i];
    for(i = j = 10; i < BLOCK_SIZE; i++)
        newTrailerBlock -> keyB[i - j] = card -> content[fromAddress + i];
   
    return newTrailerBlock;
}

/**
    Transforms a sector trailer block into a raw block

    @param trailer Trailer block to be transformed
    @return The raw block
*/
rawblock_t *buildRawBlockFromTrailerBlock(trailerblock_t *trailer)
{
    if(trailer == NULL)
        return NULL;

    rawblock_t *rawblock = calloc(1, sizeof(rawblock_t));
    
    int i, j = 0;
    for(i = 0; i < KEYA_SIZE; i++)
        rawblock -> data[j++] = trailer -> keyA[i];
    for(i = 0; i < ACCCOND_SIZE; i++)
        rawblock -> data[j++] = trailer -> accessConditions[i];
    for(i = 0; i < KEYB_SIZE; i++)
        rawblock -> data[j++] = trailer -> keyB[i];

    return rawblock;
}

/**
    Validates the data block access conditions provided, and returns its corresponding integer value

    @param options Data block options to check
    @return A value < 0 when options are invalid, a value >=0 otherwise
*/
unsigned char validateAccessCondsDataBlock(wdboptions_t *options) 
{
    unsigned char accessConds = -1;

    if(options == NULL)
        return accessConds;

    if(options -> read == KEYAB)
    {
        switch(options -> write)
        {
            case KEYAB:
                accessConds = 0;
                break;
            case KEYB:
                accessConds = 4 | (options -> isValueBlock ? 2: 0);
                break;
            case NEVER:
                accessConds = 1 + (options -> isValueBlock ? 0: 1);
                break;
            default:
                accessConds = -1;
        }
    }else if(options -> read == KEYB)
    {
        if(options -> isValueBlock)
            accessConds = -1;
        else
            switch(options -> write)
            {
                case KEYB:
                    accessConds = 3;
                    break;
                case NEVER:
                    accessConds = 5;
                    break;
                default:
                    accessConds = -1;
            }
    }else if(options -> read == NEVER)
    {
        if(options -> isValueBlock)
            accessConds = -1;
        else
            accessConds = (options -> write == NEVER ? 7: -1);
    }

    return accessConds;
}

/**
    Validates the sector trailer access conditions provided, and returns its corresponding integer value

    @param options Sector trailer block options to check
    @return A value < 0 when options are invalid, a value >=0 otherwise
*/
unsigned char validateAccessCondsTrailerBlock(wstoptions_t *options) 
{
    unsigned char accessConds = -1;

    if(options == NULL)
        return accessConds;
    
    if(options -> writeKeyA == KEYA)
    {
        if(options -> readAccessBits == KEYA && 
                options -> readKeyB == options -> writeKeyB && 
                options -> readKeyB == KEYA)
        {
            accessConds = 0 + (options -> writeAccessBits == KEYA ? 1 : 0);
            if(options -> writeAccessBits != KEYA && options -> writeAccessBits != NEVER)
                accessConds = -1;
        }
    }else if(options -> writeKeyA == KEYB)
    {
        if(options -> readAccessBits == KEYAB && 
                options -> readKeyB == NEVER && 
                options -> writeKeyB == KEYB)
        {
            accessConds = 3 + (options -> writeAccessBits == NEVER ? 1 : 0);
            if(options -> writeAccessBits != KEYB && options -> writeAccessBits != NEVER)
                accessConds = -1;
        }
    }else if(options -> writeKeyA == NEVER)
    {
        switch(options -> readAccessBits)
        {
            case KEYA:
                if(options -> writeAccessBits == NEVER && 
                        options -> writeAccessBits == options -> writeKeyB && 
                        options -> readKeyB == KEYA)
                    accessConds = 2;
                break;
            case KEYAB:
                if(options -> readKeyB == NEVER && 
                        options -> readKeyB == options -> writeKeyB)
                {
                    accessConds = 6 - (options -> writeAccessBits == KEYB ? 1 : 0);
                    if(options -> writeAccessBits != KEYB && options -> writeAccessBits != NEVER)
                        accessConds = -1;
                }
                break;
            case KEYB:
            case NEVER:
                // Not valid
                accessConds = -1;
        }
    }
    return accessConds;
}


/**
    Parses a trailer block and returns block access conditions

    @param block Trailer block to parse
    @return An access condition structure properly filled

REMEMBER configuration of access bits is
 Byte 6  |  !C2_3 !C2_2 !C2_1 !C2_0 !C1_3 !C1_2 !C1_1 !C1_0
 Byte 7  |   C1_3  C1_2  C1_1  C1_0 !C3_3 !C3_2 !C3_1 !C3_0
 Byte 8  |   C3_3  C3_2  C3_1  C3_0  C2_3  C2_2  C2_1  C2_0
 Byte 9  |                 (user data)
*/
accessconds_t *readAccessConds(trailerblock_t *block)
{
    accessconds_t *accessConds = calloc(1, sizeof(accessconds_t));
    char aux;
    int i = 3;
    unsigned char    C1  = (block -> accessConditions[1] & 0xF0) >> 4, 
            C2  = block -> accessConditions[2] & 0x0F, 
            C3  = (block -> accessConditions[2] & 0xF0) >> 4, 
            nC1 = block -> accessConditions[0] & 0x0F, 
            nC2 = (block -> accessConditions[0] & 0xF0) >> 4, 
            nC3 = block -> accessConditions[1] & 0x0F;

    //fprintf(stdout, "%x, %x; %x, %x; %x, %x\n", C1, nC1, C2, nC2, C3, nC3);
    /*fprintf(stdout, "%02x, %02x, %02x, %02x\n", block -> accessConditions[0], block -> accessConditions[1],
                                        block -> accessConditions[2], block -> accessConditions[3]);*/

    // Process each one of the blocks
    aux =   ((C1 & 0x08) >> 1) |
            ((C2 & 0x08) >> 2) |
            ((C3 & 0x08) >> 3);
    
    accessConds -> block[i--] = aux;
    
    aux =   (C1 & 0x04) |
            ((C2 & 0x04) >> 1) |
            ((C3 & 0x04) >> 2);
    
    accessConds -> block[i--] = aux;
    
    aux =   ((C1 & 0x02) << 1) |
            (C2 & 0x02) |
            ((C3 & 0x02) >> 1);
    
    accessConds -> block[i--] = aux;
    
    aux =   ((C1 & 0x01) << 2) |
            ((C2 & 0x01) >> 1) |
            (C3 & 0x01);
    
    accessConds -> block[i--] = aux;

    /*fprintf(stdout, "[%x, %x, %x, %x]\n", accessConds -> block[0], accessConds -> block[1],
                                            accessConds -> block[2], accessConds -> block[3]);*/

    // Verify whether the access conditions are well read (use negated values to this aim) 
    if(C1 != (~nC1 & 0x0F))
        fprintf(stderr, WARNING_COLOR "(!!) WARNING: C1 byte from access conditions incorrect! (value %x, negated value found %x)\n" ANSI_COLOR_RESET, C1, nC1);
    if(C2 != (~nC2 & 0x0F))
        fprintf(stderr, WARNING_COLOR "(!!) WARNING: C2 byte from access conditions incorrect! (value %x, negated value found %x)\n" ANSI_COLOR_RESET, C2, nC2);
    if(C3 != (~nC3 & 0x0F))
        fprintf(stderr, WARNING_COLOR "(!!) WARNING: C3 byte from access conditions incorrect! (value %x, negated value found %x)\n" ANSI_COLOR_RESET, C3, nC3);

    // And return results
    return accessConds;
}

/**
    Modifies the access conditions for a given mBlock

    @param oldAccessConds Old access conditions
    @param mBlock Block to be modified
    @param newConds New access conditions for the mBlock (in integer value format)
    @return A new access condition structure properly filled

REMEMBER configuration of access bits is
 Byte 6  |  !C2_3 !C2_2 !C2_1 !C2_0 !C1_3 !C1_2 !C1_1 !C1_0
 Byte 7  |   C1_3  C1_2  C1_1  C1_0 !C3_3 !C3_2 !C3_1 !C3_0
 Byte 8  |   C3_3  C3_2  C3_1  C3_0  C2_3  C2_2  C2_1  C2_0
 Byte 9  |                 (user data)
*/
accessconds_t *modifyAccessConditions(accessconds_t *oldAccessConds, 
                                            int mBlock, unsigned char newConds)
{
    if(oldAccessConds == NULL)
    {
        fprintf(stderr, "ERROR: access conditions to modify are NULL\n");
        return NULL;
    }
    if(newConds == (unsigned char)-1)
    {
        fprintf(stderr, "ERROR on new conditions for block %d, please verify them\n", mBlock);
        return NULL;
    }
    
    accessconds_t *newAccessConds = calloc(1, sizeof(accessconds_t));
    // Copy origin
    int i;
    for(i = 0; i < BLOCKSPERSHORTSECTOR; i++)
        newAccessConds -> block[i] = oldAccessConds -> block[i];
    // And modify it
    newAccessConds -> block[mBlock] = newConds;

    return newAccessConds;
}

/**
    Creates the byte values of access conditions
    
    @param accessConds Access conditions to parsed
    @param userData Value of 4th byte of access conditions (user-data value)
*/
unsigned char *buildAccessConditions(accessconds_t *accessConds, unsigned char userData)
{
    int i;

    if(accessConds == NULL)
    {
        fprintf(stderr, "ERROR: NULL pointer detected in access conditions\n");
        return NULL;
    }
    // Verify access conditions are correct for all blocks
    for(i = 0; i < ACCCOND_SIZE; i++)
        if(accessConds -> block[i] == (unsigned char)-1)
        {
            fprintf(stderr, "ERROR: block %d has invalid access conditions\n", i);
            return NULL;
        }
    
    unsigned char *accessConditions = calloc(1, sizeof(ACCCOND_SIZE));
    // First, create nibbles for C1, C2, and C3
    unsigned char nibble[3];
    // C3
    nibble[2] = (accessConds -> block[3] & 0x01) << 3 |
                (accessConds -> block[2] & 0x01) << 2 |
                (accessConds -> block[1] & 0x01) << 1 |
                (accessConds -> block[0] & 0x01);
    // C2
    nibble[1] = (accessConds -> block[3] & 0x02) << 2 |
                (accessConds -> block[2] & 0x02) << 1 |
                (accessConds -> block[1] & 0x02) |
                (accessConds -> block[0] & 0x02) >> 1;
    // C1
    nibble[0] = (accessConds -> block[3] & 0x04) << 1 |
                (accessConds -> block[2] & 0x04) |
                (accessConds -> block[1] & 0x04) >> 1 |
                (accessConds -> block[0] & 0x04) >> 2;

    /*fprintf(stdout, ".(%d, %d, %d, %d)\n", accessConds -> block[0], accessConds -> block[1],
                                            accessConds -> block[2], accessConds -> block[3]);
    fprintf(stdout, ".[%d, %d, %d]\n", nibble[0], nibble[1], nibble[2]);*/

    // Now, create the access bits properly
    accessConditions[0] = (~(nibble[1] << 4) & 0xF0) | (~nibble[0] & 0x0F);
    accessConditions[1] = (nibble[0] << 4) | (~nibble[2] & 0x0F);
    accessConditions[2] = (nibble[2] << 4) | (nibble[1] & 0x0F);
    accessConditions[3] = userData; // User-data value 

    /*fprintf(stdout, ".(%x, %x, %x, %x)\n", accessConditions[0], accessConditions[1],
                                            accessConditions[2], accessConditions[3]);*/

    // That's all folks
    return accessConditions;
}

/**
    Prints the access conditions of a block
    
    @param cardType MIFARE card type
    @param blockNum No. of block of interest
    @param accessConds Binary access conditions to parse
*/
void printAccessConditions(mfctype_t cardType, int blockNum, accessconds_t *accessConds)
{
    // Parse access conditions per block
    // Data blocks first
    int auxNumBlock = blockNum;
    if(cardType == MIFARE_CLASSIC_S70)
    {
        if(blockNum >= 0 && blockNum <= 4)
            auxNumBlock = 0;
        else if(blockNum >= 5 && blockNum <= 9)
            auxNumBlock = 1;
        else if(blockNum >= 10 && blockNum <= 14)
            auxNumBlock = 2;
        else
            auxNumBlock = 3;
    }
    
    if(auxNumBlock < 3)
    {
        printf("-. [Block %d] (%d)\t", blockNum, accessConds -> block[auxNumBlock]);
        //printf("s %d to %d]\t", i*5, i*5 + 4);
        switch(accessConds -> block[auxNumBlock])
        {
            case 0:
                printf("AB (r)\t\tAB (w)\t\tAB (i)\t\tAB (d, t, rs)\n");
                break;
            case 1:
                printf("AB (r)\t\tN (w)\t\tN (i)\t\tAB (d, t, rs)\t(*)\n");
                break;
            case 2:
                printf("AB (r)\t\tN (w)\t\tN (i)\t\tN (d, t, rs)\n");
                break;
            case 3:
                printf("B (r)\t\tB (w)\t\tN (i)\t\tN (d, t, rs)\n");
                break;
            case 4:
                printf("AB (r)\t\tB (w)\t\tN (i)\t\tN(d, t, rs)\n");
                break;
            case 5:
                printf("B (r)\t\tN (w)\t\tN (i)\tN\t(d, t, rs)\n");
                break;
            case 6:
                printf("AB (r)\t\tB (w)\t\tB (i)\t\tAB(d, t, rs)\t(*)\n");
                break;
            case 7:
                printf("N (r)\t\tN (w)\t\tN (i)\t\tN(d, t, rs)\n");
                break;
            default:
                printf("** YOU SHOULD NEVER READ THIS MESSAGE **\n");
                break;
        }
    }else{
        // Trailer block
        printf("-. [Block %d] (%d)\t", cardType == MIFARE_CLASSIC_S50 ? 3:15, accessConds -> block[auxNumBlock]);
        switch(accessConds -> block[3])
        {
            case 0:
                printf("A (kaw)\t\tA (abr)\t\tN (abw)\t\tA (kbr)\t\tA (kbw)\n");
                break;
            case 1:
                printf("A (kaw)\t\tA (abr)\t\tA (abw)\t\tA (kbr)\t\tA (kbw)\n");
                break;
            case 2:
                printf("N (kaw)\t\tA (abr)\t\tN (abw)\t\tA (kbr)\t\tN (kbw)\n");
                break;
            case 3:
                printf("B (kaw)\t\tAB (abr)\t\tB (abw)\t\tN (kbr)\t\tB (kbw)\n");
                break;
            case 4:
                printf("B (kaw)\t\tAB (abr)\t\tN (abw)\t\tN (kbr)\t\tB (kbw)\n");
                break;
            case 5:
                printf("N (kaw)\t\tAB (abr)\t\tB (abw)\t\tN (kbr)\t\tN (kbw)\n");
                break;
            case 6:
            case 7:
                printf("N (kaw)\t\tAB (abr)\t\tN (abw)\t\tN (kbr)\t\tN (kbw)\n");
                break;
            default:
                printf("** YOU SHOULD NEVER READ THIS MESSAGE **\n");
                break;
        }
    }


}

/**
    Prints a MIFARE card in binary mode
    
    @param card MIFARE card to be printed
*/
void dumpBinaryCard(mfclassic_t *card)
{
    fprintf(stdout, "Card info -- [%s", 
                    getCardTypeString(card -> cardType));
    int i, maxBytes = getCardTypeMaxBytes(card -> cardType);

    printf("; ");
    printManufacturerBlock(buildManufacturerBlock(card));
    printf("]\nContent (full dump):\n");

    for(i = 0; i < maxBytes;)
    {
	// print escape colors
    	if(!i)
		fprintf(stdout, ANSI_COLOR_CYAN);
	else if(i == (UID_SIZE + BCC_SIZE + MANUFACTURERDATA_SIZE))
		fprintf(stdout, ANSI_COLOR_RESET);
	else if(i % (BLOCK_SIZE*BLOCKSPERSHORTSECTOR) == BLOCK_SIZE*(BLOCKSPERSHORTSECTOR - 1))
		fprintf(stdout, KEYA_COLOR);
	else if(i % (BLOCK_SIZE*BLOCKSPERSHORTSECTOR) == (BLOCK_SIZE*(BLOCKSPERSHORTSECTOR - 1) + KEYA_SIZE))
		fprintf(stdout, ACCCOND_COLOR);
	else if(i % (BLOCK_SIZE*BLOCKSPERSHORTSECTOR) == (BLOCK_SIZE*(BLOCKSPERSHORTSECTOR - 1) + KEYA_SIZE + ACCCOND_SIZE))
		fprintf(stdout, KEYB_COLOR);
	else if(!(i % (BLOCK_SIZE*BLOCKSPERSHORTSECTOR)))
		fprintf(stdout, ANSI_COLOR_RESET);
		
        fprintf(stdout, "%02x", card -> content[i++]);
        if(!(i % (BLOCK_SIZE*BLOCKSPERSHORTSECTOR)))
            fprintf(stdout, "\n");
    }
    fprintf(stdout, ANSI_COLOR_RESET "["ANSI_COLOR_CYAN "UID+BCC+Manufacturer data; " KEYA_COLOR "keys A; " ACCCOND_COLOR "Access conditions; " KEYB_COLOR "keys B" ANSI_COLOR_RESET"]\n");
}

/**
    Prints the access conditions of fromBlock to toBlock of a sector
    
    @param card MIFARE card to be read
    @param sector Sector of interest
    @param fromBlock No. of block where init the printing
    @param toBlock No. of block where finish the printing
*/
void dumpBlocksConditions(mfclassic_t *card, int sector, int fromBlock, int toBlock)
{
    int block = fromBlock;
    if(!(validateSectorBlock(card -> cardType, sector, fromBlock) && 
            validateSectorBlock(card -> cardType, sector, toBlock) && fromBlock <= toBlock))
        return;

    // Get trailer block, and read access conditions
    int initAddr = computeSectorTrailerInitAddress(card -> cardType, sector);
    printf("[Sector %d] @%d\n", sector, initAddr);
    accessconds_t *accessConds = readAccessConds(buildTrailerBlock(card, initAddr));
    
    for(; block <= toBlock; block++)
            printAccessConditions(card -> cardType, block, accessConds);

}

/**
    Reads and prints the access conditions of the (sector, block)
    
    @param card MIFARE card to be read
    @param sector Sector of interest
    @param block No. of block whose access conditions will be printed
*/
void readBlockConditions(mfclassic_t *card, int sector, int block)
{
    // Validate block and sector
    if(card == NULL)
    {
        fprintf(stderr, "readBlockConditions: NULL card detected, exiting\n");
        return;
    }
    return dumpBlocksConditions(card, sector, block, block);
}

/**
    Reads and prints the access conditions of the sector trailer of a given sector
    
    @param card MIFARE card to be read
    @param sector No. of sector whose sector trailer's access conditions will be printed
*/
void readSectorTrailerConditions(mfclassic_t *card, int sector)
{
    // Validate block and sector
    if(card == NULL)
    {
        fprintf(stderr, "readSectorTrailerConditions: NULL card detected, exiting\n");
        return;
    }
    readBlockConditions(card, sector, 
        (sector < (card -> cardType == MIFARE_CLASSIC_S50 ? _1KSECTORS : _4KSHORTSECTORS) ? 
                                        (BLOCKSPERSHORTSECTOR - 1):
                                        (BLOCKSPERLONGSECTOR - 1)));
    printTrailerBlock(buildTrailerBlock(card, computeSectorTrailerInitAddress(card -> cardType, sector)));
}

/**
    Reads and prints the access conditions of *all* blocks of a given MIFARE card
    
    @param card MIFARE card to be read
*/
void readAllConditions(mfclassic_t *card)
{
    // Validate block and sector
    if(card == NULL)
    {
        fprintf(stderr, "readAllConditions: NULL card detected, exiting\n");
        return;
    }

    // Dump all card, parsing it properly
    int maxSectors = getMaxSectors(card -> cardType);
    int sector;

    for(sector = 0; sector < maxSectors; sector++)
    {
        int maxBlocks = BLOCKSPERSHORTSECTOR;
        if(card -> cardType == MIFARE_CLASSIC_S70 && sector >= _4KSHORTSECTORS)
            maxBlocks = BLOCKSPERLONGSECTOR;
        // Iterate on blocks
        dumpBlocksConditions(card, sector, 0, maxBlocks - 1);

    }
    printLegend(TRUE, TRUE);
}

/**
    Performs a binary copy of a MIFARE card
    
    @param dumpCard MIFARE card to be copied
    @return The binary copy of dumpCard
*/
mfclassic_t *copyDumpCard(mfclassic_t *dumpCard)
{
    if(dumpCard == NULL)
    {
        fprintf(stderr, "ERROR card to be copied is NULL!\n");
        return NULL;
    }

    mfclassic_t *newCard = calloc(1, sizeof(mfclassic_t));
    int bytesToCopy = getCardTypeMaxBytes(dumpCard -> cardType);
    // Allocate memory
    newCard -> content = calloc(1, sizeof(unsigned char)*bytesToCopy);
    // And copy it
    newCard -> cardType = dumpCard -> cardType;
    int i;
    for(i = 0; i < bytesToCopy; i++)
        newCard -> content[i] = dumpCard -> content[i];
    
    return newCard;
}

/**
    Writes the content of block (BLOCK_SIZE) into a given card
    XXX Note that *no checking of correct initAddressBlock* is performed!
    XXX Only it is validated memory bounds
    
    @param card MIFARE card to modify
    @param initAddressBlock Init address of block to modify
    @param block Content of the block to be written
    @return TRUE when no errors raised, FALSE otherwise
*/
int writeBlockToCardContent(mfclassic_t *card, int initAddressBlock, rawblock_t *block)
{
    if(card == NULL || block == NULL)
        return FALSE;

    //fprintf(stdout, "Max bytes: %d; init address %d\n", getCardTypeMaxBytes(card -> cardType), initAddressBlock);

    // Validat address to be written
    if(getCardTypeMaxBytes(card -> cardType) < (initAddressBlock + BLOCK_SIZE))
    {
        fprintf(stderr, "ERROR init address to write invalid\n");
        return FALSE;
    }

    int i;
    for(i = 0; i < BLOCK_SIZE; i++)
    {
        //fprintf(stdout, "Changing %x to %x...\n", card -> content[initAddressBlock + i], block -> data[i]);
        card -> content[initAddressBlock + i] = block -> data[i];
    }

    return TRUE;
}

/**
    Modifies the given options for a given (sector, block) in a given card
    
    @param oldCard Previous MIFARE card
    @param newCard MIFARE card containing the modification
    @param sector No. of sector whose block options will be modified
    @param block No. of block inside the sector to be modified
    @param options Block options to set
    @param newAccessConds New access conditions to be set
    @return TRUE when no errors raised, FALSE otherwise
*/
int modifyBlockConditions(mfclassic_t *oldCard, mfclassic_t *newCard,
                                int sector, int block, unsigned char newAccessConds)
{
    // Compute init address of sector trailer containing the block to be modified
    int initAddress = computeSectorTrailerInitAddress(oldCard -> cardType, sector);
    trailerblock_t *trailerBlock = buildTrailerBlock(oldCard, initAddress);

    accessconds_t   *currentConds = readAccessConds(trailerBlock);
    fprintf(stdout, "Changing conditions [%d, %d] (%d -> %d)...\n", 
                                    sector, block, currentConds -> block[block], newAccessConds);
    accessconds_t   *newConds = modifyAccessConditions(currentConds, block, newAccessConds);

    /*fprintf(stdout, "Conditions: %02x, %02x, %02x, %02x\n", newConds -> block[0], newConds -> block[1],
                                                            newConds -> block[2], newConds -> block[3]);*/

    unsigned char *newAccessConditions = buildAccessConditions(newConds, 
                                            trailerBlock -> accessConditions[ACCCOND_SIZE - 1]);
    // Change conditions
    int i;
    for(i = 0; i < ACCCOND_SIZE; i++)
        trailerBlock -> accessConditions[i] = newAccessConditions[i];

    // Create raw block, and modify content
    rawblock_t *rawBlock = buildRawBlockFromTrailerBlock(trailerBlock);
    if(!writeBlockToCardContent(newCard, initAddress, rawBlock))
    {
        fprintf(stderr, "ERROR when writing block %d (%d) to card content\n", block, sector);
        releaseCard(newCard);
        newCard = NULL;
        return FALSE;
    }

    // Release memory
    free(newAccessConditions);
    free(currentConds);
    // Release blocks
    releaseRawBlock((rawblock_t *)trailerBlock);
    releaseRawBlock(rawBlock);

    return TRUE;
}

/**
    Validates the options for a given (sector, block) in a given card
    
    @param cardType MIFARE card type to validate
    @param sector No. of sector whose block options will be modified
    @param block No. of block inside the sector to be modified
    @param options Block options to validate
    @param dataBlock Boolean to determine whether the block is a data or a sector trailer block
    @return An integer >= 0 representing the new access conditions when options 
                are valid and no errors raised, -1 otherwise
*/
unsigned char validateParametersModification(mfctype_t cardType, 
                                                int sector, int block, 
                                                void *options, int dataBlock)
{
    unsigned char newAccessConds = (dataBlock ?
                            validateAccessCondsDataBlock((wdboptions_t *) options) : 
                            validateAccessCondsTrailerBlock((wstoptions_t *) options));
    if(newAccessConds == 0xFF)
    {
        fprintf(stderr, "ERROR invalid options for %s block detected.\n", (dataBlock ? "data":"sector trailer"));
        return -1;
    }

    if(options == NULL)
    {
        fprintf(stderr, "ERROR NULL options for data blocks detected.\n");
        return -1;
    }
    
    // Validate block
    if(dataBlock && !isDataBlock(cardType, sector, block))
    {
        fprintf(stderr, "ERROR block %d in sector %d is not a data block\n", block, sector);
        return -1;
    }else if(!dataBlock && !isTrailerBlock(cardType, sector, block))
    {
        fprintf(stderr, "ERROR block %d in sector %d is not a trailer block\n", block, sector);
        return -1;
    }
    return newAccessConds;
}

/**
    Modifies the options of a given (sector, block) in a given card, and returns the modified card
    
    @param card MIFARE card to modify
    @param sector No. of sector whose data block options will be modified
    @param block No. of block inside the sector to be modified
    @param options Data block options to set 
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyDataBlockOptions(mfclassic_t *card, int sector, int block, wdboptions_t *options)
{
    unsigned char newAccessConds = validateParametersModification(card -> cardType, sector, block, options, TRUE);
   
    if(newAccessConds == 0xFF)
        return NULL;

    mfclassic_t *newCard = copyDumpCard(card);
    if(newCard != NULL && modifyBlockConditions(card, newCard, sector, block, newAccessConds))
        return newCard;
    else
    {
        if(newCard != NULL)
            releaseCard(newCard);

        return NULL;
    }
}

/**
    Modifies the options of a given sector trailer in a given card, and returns the modified card
    
    @param card MIFARE card to modify
    @param sector No. of sector whose sector trailer options will be modified
    @param options Sector trailer options to set 
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyTrailerBlockOptions(mfclassic_t *card, int sector, wstoptions_t *options)
{
    int numBlock = getSectorTrailerBlockNum(card -> cardType, sector);
    unsigned char newAccessConds = validateParametersModification(card -> cardType, sector, numBlock, options, FALSE);
   
    if(newAccessConds == 0xFF)
        return NULL;

    mfclassic_t *newCard = copyDumpCard(card);
    if(newCard != NULL && modifyBlockConditions(card, newCard, sector, numBlock, newAccessConds))
        return newCard;
    else{
        if(newCard != NULL)
            releaseCard(newCard);
        return NULL;
    }
}

/**
    Modifies the options of all data blocks in a given card, and returns the modified card
    
    @param card MIFARE card to modify
    @param options Data block options to set 
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyAllDataBlocksOptions(mfclassic_t *card, wdboptions_t *options)
{
    unsigned char newAccessConds = validateParametersModification(card -> cardType, 0, 0, options, TRUE);
   
    if(newAccessConds == 0xFF)
        return NULL;

    mfclassic_t *newCard = copyDumpCard(card);
    if(newCard == NULL)
        return NULL;
    
    int nSectors = getMaxSectors(card -> cardType), auxSector, auxBlock;
    for(auxSector = 0; auxSector < nSectors; auxSector++)
    {
        int maxBlocks = BLOCKSPERSHORTSECTOR - 1;
        if(auxSector >= _4KSHORTSECTORS)
            maxBlocks = BLOCKSPERLONGSECTOR - 1;

        for(auxBlock = 0; auxBlock < maxBlocks; auxBlock++)
            if(!modifyBlockConditions(newCard, newCard, auxSector, auxBlock, newAccessConds)){
                    releaseCard(newCard);
                    return NULL;
                }
    }

    return newCard;
}

/**
    Modifies the options of all sector trailers in a given card, and returns the modified card
    
    @param card MIFARE card to modify
    @param options Sector trailer options to set 
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyAllTrailerBlocksOptions(mfclassic_t *card, wstoptions_t *options)
{
    unsigned char newAccessConds = validateParametersModification(card -> cardType, 0, 0, options, FALSE);
   
    if(newAccessConds == 0xFF)
        return NULL;
    
    mfclassic_t *newCard = copyDumpCard(card);
    if(newCard == NULL)
        return NULL;
    
    int nSectors = getMaxSectors(card -> cardType), auxSector;
    for(auxSector = 0; auxSector < nSectors; auxSector++)
    {
        int lastBlock = BLOCKSPERSHORTSECTOR - 1;
        if(auxSector >= _4KSHORTSECTORS)
            lastBlock = BLOCKSPERLONGSECTOR - 1;

        if(!modifyBlockConditions(newCard, newCard, auxSector, lastBlock, newAccessConds)){
            releaseCard(newCard);
            return NULL;
        }
    }

    return newCard;
}

/**
    Modifies the access key of a given sector, and returns the modified card
    
    @param card MIFARE card to modify
    @param sector No. of sector whose sector trailer options will be modified
    @param key Key to modify, either KEY_A or KEY_B
    @param hexkey 6-byte hex string -- assumed to be valid!
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifySectorKey(mfclassic_t *card, int sector, accesstype_t key, unsigned char *hexkey)
{
    if(!validateSector(card -> cardType, sector))
        return NULL;
        
    mfclassic_t *newCard = copyDumpCard(card);
    if(newCard == NULL)
        return NULL;
    
    int initAddress = computeSectorTrailerInitAddress(card -> cardType, sector);
    trailerblock_t *trailerBlock = buildTrailerBlock(card, initAddress);
    
    unsigned char *ptrKey;
    switch(key)
    {
        case KEYA:
            ptrKey = trailerBlock -> keyA;
            break;
        case KEYB:
            ptrKey = trailerBlock -> keyB;
            break;
        default:
            // Error!
            fprintf(stderr, "modifySectorKey: Key to modify (%d) not recognized, please check!\n", key);
            releaseCard(newCard);
            newCard = NULL;
    }
    
    if(newCard != NULL)
    {
        // Change key
        for(int i = 0; i < KEYA_SIZE; i++)
            ptrKey[i] = hexkey[i];
        
        // Create raw block, and modify content
        rawblock_t *rawBlock = buildRawBlockFromTrailerBlock(trailerBlock);
        if(!writeBlockToCardContent(newCard, initAddress, rawBlock))
        {
            fprintf(stderr, "ERROR when writing sector trailer in %d to card content\n", sector);
            releaseCard(newCard);
            newCard = NULL;
        }
        releaseRawBlock(rawBlock);
    }

    // Release blocks
    releaseRawBlock((rawblock_t *)trailerBlock);

    return newCard;
}


/**
    Modifies the access key of all sectors, and returns the modified card
    
    @param card MIFARE card to modify
    @param key Key to modify, either KEYA or KEYB
    @param hexkey 6-byte hex string -- assumed to be valid!
    @return A new MIFARE card with the modifications
*/
mfclassic_t *modifyAllSectorsKey(mfclassic_t *card, accesstype_t key, unsigned char *hexkey)
{
    mfclassic_t *newCard = copyDumpCard(card);
    if(newCard == NULL)
        return NULL;
   
    int maxSectors = getMaxSectors(card -> cardType);
    for(int nSec = 0; nSec < maxSectors; nSec++)
    {
        int initAddress = computeSectorTrailerInitAddress(card -> cardType, nSec);
        trailerblock_t *trailerBlock = buildTrailerBlock(card, initAddress);
    
        unsigned char *ptrKey;
        switch(key)
        {
            case KEYA:
                ptrKey = trailerBlock -> keyA;
                break;
            case KEYB:
                ptrKey = trailerBlock -> keyB;
                break;
            default:
                // Error!
                fprintf(stderr, "modifySectorKey: Key to modify (%d) not recognized, please check!\n", key);
                releaseCard(newCard);
                releaseRawBlock((rawblock_t *)trailerBlock);
                return NULL;
        }
    
        // Change key
        for(int i = 0; i < KEYA_SIZE; i++)
            ptrKey[i] = hexkey[i];
        
        // Create raw block, and modify content
        rawblock_t *rawBlock = buildRawBlockFromTrailerBlock(trailerBlock);
        if(!writeBlockToCardContent(newCard, initAddress, rawBlock))
        {
            fprintf(stderr, "ERROR when writing sector trailer in %d to card content\n", nSec);
            releaseCard(newCard);
            releaseRawBlock(rawBlock);
            releaseRawBlock((rawblock_t *)trailerBlock);
            return NULL;
        }
        
        // Release blocks
        releaseRawBlock(rawBlock);
        releaseRawBlock((rawblock_t *)trailerBlock);
    }

    return newCard;
}


/**
    Writes a MIFARE card to a file
    
    @param outfilename Filename of output file
    @param card MIFARE card to dump
*/
void writeCardToFile(char *outfilename, mfclassic_t *card)
{
    FILE *f = fopen(outfilename, "w");

    if(f == NULL)
        fprintf(stderr, "ERROR when opening file %s to write\n", outfilename);
    else{
        if(card == NULL)
        {
            fprintf(stderr, "ERROR when writing card to file: NULL card detected!\n");
            return;
        }

        fprintf(stdout, "Writing dump card to file [%s]...\n", outfilename);

        // Write all the content to the file
        int bytesToWrite = getCardTypeMaxBytes(card -> cardType);
        fwrite(card -> content, sizeof(unsigned char), bytesToWrite, f);
        
        // Close the file
        fclose(f);
        fprintf(stdout, "Done.\n");
    }
}
